package com.puravida
import spock.lang.Specification

class WcParseSpec extends Specification {

    void "test one word"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream("hi".bytes) )

        then: "only one word"
        ret.size() == 1
        ret[0].key == 'hi'
        ret[0].value== 1
    }

    void "test one word with punctuations"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream("(. hi !.,)".bytes) )

        then: "only one word"
        ret.size() == 1
        ret[0].key == 'hi'
        ret[0].value== 1
    }

    void "test #word with punctuations are #value presents"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream("(. hi !. word,)".bytes) )

        then: "only two word"
        ret[0].key == 'hi'
        ret[0].value== 1
        ret[1].key == 'word'
        ret[1].value== 1
    }

    void "test some #word in a fixed lore lipsum"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream(
"""Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
""".bytes) )

        then: "match words"
        ret.size() == 65
        ret.first().key == 'the'
        ret.first().value== 6

        where:
        word    | index | count
        'The'   | 0     | 6
        'Ipsum' | 1     | 4
        'centuries' | 23     | 1
    }

    void "test a #repeat lore lipsum"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        String text =
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
""".multiply(repeat)

        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream(text.bytes) )

        then: "match words"
        ret.size() == 65
        ret.first().key == 'the'
        ret.first().value== 6*repeat

        ret[23].key == 'centuries'
        ret[23].value == 1*repeat

        where:
        repeat << [1, 10, 20, 50]
    }



    void "compare two similar lore lipsum"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse two lore lipsum"
        List<Map.Entry<String,Integer>> ret1 = parse.parse( new ByteArrayInputStream(
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
""".bytes) )

        List<Map.Entry<String,Integer>> ret2 = parse.parse( new ByteArrayInputStream(
                """Ipsum Lorem is simply dummy text of the printing and typesetting industry. 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
""".bytes) )

        then: "match words"
        ret1.eachWithIndex{ Map.Entry<String, Long> entry, int i ->
            assert ret2[i].key == entry.key
            assert ret2[i].value == entry.value
        }
    }

    void "compare two similar lore lipsum with a more Ipsum"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse two lore lipsum"
        List<Map.Entry<String,Integer>> ret1 = parse.parse( new ByteArrayInputStream(
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
""".bytes) )

        List<Map.Entry<String,Integer>> ret2 = parse.parse( new ByteArrayInputStream(
                """Ipsum Lorem is simply dummy text of the printing and typesetting industry. 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 

Ipsum

""".bytes) )

        then: "match words"
        ret1.find{ it.key=='Ipsum'}.value == ret2.find{ it.key == 'Ipsum'}.value - 1
    }


    void "test a random lore lipsum"(){
        given: "a parse"
        WcParse parse = new WcParse()

        and: "a random text"
        int size = 10+ new Random().nextInt(10)
        String rnd = 'a'.multiply(20)

        String text = """Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book.
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
"""
        (0..size-1).each{
            text = "$text $rnd "
        }

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parse( new ByteArrayInputStream(text.bytes) )

        then: "match random word"
        ret.size() == 66
        ret.first().key == rnd
        ret.first().value == size
    }

    void "test one word in groovy"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parseGroovy( new StringReader("hi") )

        then: "only one word"
        ret.size() == 1
        ret[0].key == 'hi'
        ret[0].value== 1
    }

    void "test one word with punctuations in groovy"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parseGroovy( new StringReader("(. hi !.,)") )

        then: "only one word"
        ret.size() == 1
        ret[0].key == 'hi'
        ret[0].value== 1
    }

    void "test some #word in a fixed lore lipsum in Groovy"(){
        given: "a parse"
        WcParse parse = new WcParse()

        when: "parse a single word"
        List<Map.Entry<String,Integer>> ret = parse.parseGroovy( new StringReader(
                """Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged. It was popularised in the 1960s with the release 
of Letraset sheets containing Lorem Ipsum passages, and more recently with 
desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
""") )

        then: "match words"
        ret.size() == 65
        ret.first().key == 'the'
        ret.first().value== 6

        where:
        word    | index | count
        'The'   | 0     | 6
        'Ipsum' | 1     | 4
        'centuries' | 20     | 1
    }

    void "test a really big big stream in groovy"(){
        given: "a parse"
        WcParse parse = new WcParse()

        String msg = ''
        ('a'..'z').eachWithIndex{ l , i ->
            msg+= ' '+"$l ".multiply( i * 1000)
        }

        when: "parse a big big stream"
        List<Map.Entry<String,Integer>> ret = parse.parseGroovy( new StringReader(msg) )

        then:
        ret.size() == 25
        ret[0].key == 'z'
        ret[0].value== 1000*25
    }


}
