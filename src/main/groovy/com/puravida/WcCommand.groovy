package com.puravida

class WcCommand {

    static void main(String[] args) throws Exception {

        if( args.length != 1){
            System.err.println("Missing file path argument")
            System.exit(-1)
            return
        }

        new WcCommand().run(args[0])
    }

    void run(String fileName) {

        File file = new File(fileName)
        if( file.exists() == false ){
            System.err.println("File $fileName not found")
            System.exit(-1)
            return
        }

        try {
            List<Map.Entry<String, Integer>> result = new WcParse().parse(file.newInputStream())
            result.each {
                println "$it.key: $it.value"
            }
        }catch( IOException ioe ){
            System.err.println(ioe.message)
            System.exit(-1)
            return
        }
    }
}
