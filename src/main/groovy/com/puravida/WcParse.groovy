package com.puravida

import java.util.function.Function
import java.util.regex.Matcher
import java.util.stream.Collectors

class WcParse {

    //tag::collect[]
    private void collectWord(Map<String,Integer> words, String word){
        if( word.length() ) {
            if (words.containsKey(word) == false) {
                words[word] = 1
            } else {
                words[word] += 1
            }
        }
    }
    //end::collect[]

    //tag::groovy[]
    List<Map.Entry<String,Integer>> parseGroovy(Reader reader){
        Map<String,Integer> words = [:]
        String last = ''
        reader.withReader{ Reader r->   //<1>
            int ch
            while( ( ch = r.read()) != -1) {        //<2>
                char c = ch as char
                if (c =~ '[a-zA-Z]') {  //<3>
                    last += c
                } else {
                    collectWord(words, last)  //<4>
                    last = ''
                }
            }
            collectWord(words, last)
        }

        words.entrySet().sort{ Map.Entry<String,Integer>a, Map.Entry<String,Integer>b->   //<5>
            b.value <=> a.value ?: a.key <=> b.key
        }
    }
    //end::groovy[]


    List<Map.Entry<String,Integer>> parse( InputStream inputStream){

        //tag::map[]
        Map<String,Long> map = new BufferedReader(new InputStreamReader(inputStream)).lines()
                .flatMap{ line ->
                    Arrays.stream( (line =~ '([a-zA-Z]+)') )    //<1>
                }
                .filter { Matcher matcher->
                    matcher.find() //<2>
                }
                .flatMap{ Matcher matcher ->
                    matcher.collect{ List list -> list[0] }.stream() //<3>
                }
                .map{  String word ->
                    new AbstractMap.SimpleEntry(word, 1 as int) //<4>
                }
                .collect(
                        Collectors.groupingBy( (Function) { AbstractMap.SimpleEntry entry ->
                            entry.key
                        }, Collectors.counting()) //<5>
                )
        //end::map[]

        //tag::list[]
        List<Map.Entry<String,Integer>> ret = map.entrySet().stream().
                sorted( Comparator.comparing( (Function) { Map.Entry<String,Integer> entry -> //<1>
                    -1 * entry.value
                }).thenComparing((Function) { Map.Entry<String,Integer> entry ->  //<2>
                    entry.key
                })).collect( Collectors.toList() )  //<3>
        //end::list[]

        ret
    }
}
